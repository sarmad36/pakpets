let dbConfig      = require("../Utilities/mysqlConfig");
let randomstring  = require("randomstring");
const crypto      = require('crypto'),
      algorithm   = 'aes-256-ctr',
      password    = 'd6F3Efeq';
// const algorithm   = 'aes-256-cbc';
const key         = crypto.randomBytes(32);
const iv          = crypto.randomBytes(16);
var nodemailer    = require('nodemailer');
const bcrypt      = require('bcrypt');
const hbs         = require('nodemailer-express-handlebars');
Handlebars        = require('handlebars');
var fs            = require('fs');
var insertedID    = 0;

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'sarmad366@gmail.com',
    pass: 'Uznoor_11'
  }
});


let getListing = (criteria, callback) => {
   dbConfig.getDB().query(`select l.* ,loc.name as locationName, cat.name as categoryName, GROUP_CONCAT(im.name ORDER BY im.name) AS images from sitter l LEFT JOIN location loc ON loc.id = l.location LEFT JOIN categories cat ON cat.id = l.category JOIN sitterimages im ON im.sitter_id = l.id where `+criteria.where+ ` GROUP BY l.id`,criteria, callback);
}


let createListing = (dataToSet, callback) => {

   var r          = new Object();
   var htmlToSend = '';
   if (dataToSet.user_id == 0) {
      var hw         = randomstring.generate(8);
      var pw         = bcrypt.hashSync(hw, 10);
      var password   = pw;

      dbConfig.getDB().query("SELECT id, email FROM users WHERE email = '"+dataToSet.seller_email+"'", function (err, result, fields) {
       if (err) throw err;
       if (result === undefined || result.length == 0) {
          dbConfig.getDB().query('INSERT INTO `users` SET ?', {name: dataToSet.seller_name, email: dataToSet.seller_email, userName:dataToSet.seller_email, password:password, phone:dataToSet.seller_phone, dateJoined:dataToSet.date_added, type: 3 }, function(err, result) {
             if (err) throw err;

             var username     = dataToSet.seller_email;
             var password     = hw;
             var source       = fs.readFileSync('./emial_templates/newuserListing.hbs', 'utf8');
             var template     = Handlebars.compile(source);
             var mailOptions  = {
                from          : 'sarmad366@gmail.com',
                to            : 'sarmad366@gmail.com',
                subject       : 'Your Add is recieved by pakpets.pk please check your email',
                html          : template({username, password})
             };

             dataToSet.user_id = result.insertId;
             dbConfig.getDB().query("insert into sitter set ? ", dataToSet, function(err, result) {
               if (err){
                  r.res = err;
                  r.insertedID = insertedID;
                  callback(r);
               }
               insertedID = result.insertId;
               transporter.sendMail(mailOptions, function(error, info){
                  if (error) {
                     console.log("mail error",error);
                     r.res = error;
                     r.insertedID = insertedID;
                     callback(err);
                  } else {
                     console.log('Email sent: ' + info.response);
                     r.res = true;
                     r.insertedID = insertedID;
                     callback(r);
                  }
               });
            });
          });
       }
       else {
          var username     = dataToSet.seller_email;
          var password     = "use yuor old password";
          var source       = fs.readFileSync('./emial_templates/olduserListing.hbs', 'utf8');
          var template     = Handlebars.compile(source);
          var mailOptions2 = {
             from          : 'sarmad366@gmail.com',
             to            : 'sarmad366@gmail.com',
             subject       : 'Your Add is Recieved and is in review',
             html          : template({username, password})
          };
          dataToSet.user_id = result[0].id;
          dbConfig.getDB().query("insert into sitter set ? ", dataToSet, function(err, result) {
            if (err){
               r.res = err;
               callback(r);
            }

            insertedID = result.insertId
            transporter.sendMail(mailOptions2, function(error, info){
               if (error) {
                  console.log("mail error",error);
                  r.res = false;
                  callback(r);
               } else {
                  console.log('Email sent: ' + info.response);
                  r.res = true;
                  r.insertedID = insertedID;
                  callback(r);
               }
            });
         });
       }
     });
   }


}

let addListingImages = (dataToSet, listingID, callback) => {
   // console.log("insert into ,mylistings set ? ", dataToSet,'sarmad');
   for (var i = 0; i < dataToSet.length; i++) {
      var name    = dataToSet[i].saved;
      // dbConfig.getDB().query('INSERT INTO `sitterimages` SET ?', {name: name, path: 'null', listing_id:listingID}, callback);
      dbConfig.getDB().query('INSERT INTO `sitterimages` SET ?', {name: name, path: 'null', sitter_id:listingID}, function(err, result) {
         if (err) callback(err);
         // if (err) throw err;

         console.log("inserted id IMG",result.insertId);
      });
   }
   var ret = true;
   callback(ret);
}

let editListing   = (dataToSet,listingID, callback) => {
   var r          = new Object();
   let sql = `UPDATE sitter
           SET title = ?, category = ?, price = ?, age = ?, gender = ?, location = ?, address = ?, description = ?, seller_name = ?, seller_phone = ?, seller_email = ?, tags = ?, date_modified = ?, status = ?, user_id = ?
           WHERE id = ?`;
  // var query = dbConfig.getDB().query(sql, dataToSet , function(err, result) {});
  // console.log(query.sql);
   dbConfig.getDB().query(sql, dataToSet, function(err, result) {
     if (err){
        console.log("error",err);
        r.res = err;
        callback(r);
     }
     else{
       r.res = true;
       r.insertedID = listingID;
       var sql2 = "DELETE FROM sitterimages WHERE sitter_id = "+listingID;
       dbConfig.getDB().query(sql2, function (err, result) {
          if (err){
             console.log("error",error);
             r.res = err;
             callback(r);
          }
          else{
             callback(r);
          }
       });

     }

 });
}

let getUserListing = (criteria, callback) => {

   dbConfig.getDB().query(`select l.*, GROUP_CONCAT(im.name ORDER BY im.name) AS images from sitter l LEFT JOIN sitterimages im ON im.sitter_id = l.id where `+criteria.where+ ` GROUP BY l.id ORDER BY id`,criteria, callback);
}

let getListingbyID = (criteria, callback) => {
   dbConfig.getDB().query(`select l.* ,loc.name as locationName, cat.name as categoryName, GROUP_CONCAT(im.name ORDER BY im.name) AS images, GROUP_CONCAT(im.id ORDER BY im.name) AS imagesID from sitter l LEFT JOIN location loc ON loc.id = l.location LEFT JOIN categories cat ON cat.id = l.category JOIN sitterimages im ON im.sitter_id = l.id where `+criteria.where+ ` GROUP BY l.id`,criteria, callback);
}

let deleteListing = (criteria, callback) => {
   let conditions = "";
   dbConfig.getDB().query(`delete from sitter where id = `+criteria.id.delData);

}

let approveListing = (dataToSet, callback) => {
   dbConfig.getDB().query(`UPDATE sitter SET status = ? WHERE id = ?`,dataToSet,callback);

}

let pauseListing = (dataToSet, callback) => {
   dbConfig.getDB().query(`UPDATE sitter SET status = ? WHERE id = ?`,dataToSet,callback);

}


module.exports = {
   createListing     : createListing,
   addListingImages  : addListingImages,
   editListing       : editListing,
   getUserListing    : getUserListing,
   getListingbyID    : getListingbyID,
   deleteListing     : deleteListing,
   pauseListing      : pauseListing,
   getListing        : getListing
}
