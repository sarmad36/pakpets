let dbConfig      = require("../Utilities/mysqlConfig");
let randomstring  = require("randomstring");
const crypto      = require('crypto'),
      algorithm   = 'aes-256-ctr',
      password    = 'd6F3Efeq';
// const algorithm   = 'aes-256-cbc';
const key         = crypto.randomBytes(32);
const iv          = crypto.randomBytes(16);
var nodemailer    = require('nodemailer');
const bcrypt      = require('bcrypt');
const hbs         = require('nodemailer-express-handlebars');
Handlebars        = require('handlebars');
var fs            = require('fs');

let getUserRecord = (criteria, callback) => {
   // console.log(`select * from users`,criteria);
   dbConfig.getDB().query(`select * from users`,criteria, callback);
}

let getUserbyID = (criteria, callback) => {
   // console.log('query',`select l.* ,loc.name as locationName, cat.name as categoryName, GROUP_CONCAT(im.name ORDER BY im.name) AS images, GROUP_CONCAT(im.id ORDER BY im.name) AS imagesID from mylistings l LEFT JOIN location loc ON loc.id = l.location LEFT JOIN categories cat ON cat.id = l.category JOIN myimages im ON im.listing_id = l.id where `+criteria.where+ ` GROUP BY l.id`);
   dbConfig.getDB().query(`select * from users where `+criteria.where ,criteria, callback);
}

let deleteUser = (criteria, callback) => {
   let conditions = "";
   dbConfig.getDB().query(`delete from users where id = `+criteria.id.delData);
}


let createUser = (dataToSet, callback) => {
   var r                = new Object();
   var hw               = dataToSet.password;
   var pw               = bcrypt.hashSync(hw, 10);
   dataToSet.password   = pw;
   dbConfig.getDB().query("SELECT id, email FROM users WHERE email = '"+dataToSet.email+"'", function (err, result, fields) {
      // console.log("result OUTSIDE",result);
      if(result === undefined || result.length == 0){
         // console.log("result un defined",result);
         dbConfig.getDB().query("insert into users set ? ", dataToSet, function (err, result, fields) {
            if (err){
               r.res = false;
               // r.insertedID = insertedID;
               callback(r);
            }
            else {
               r.res = true;
               // r.insertedID = insertedID;
               callback(r);
            }


         });

      }
      else {
         // console.log("result",result);
         r.res == 'exist';
         callback(r);
      }
   });

}

let editUser      = (dataToSet,listingID, callback) => {
   var r          = new Object();
   let sql = `UPDATE users
           SET name = ?, email = ?, phone = ?, type = ?, datemodified = ?
           WHERE id = ?`;
  // var query = dbConfig.getDB().query(sql, dataToSet , function(err, result) {});
  // console.log(query.sql);
   dbConfig.getDB().query(sql, dataToSet, function(err, result) {
     if (err){
        console.log("error",err);
        r.res = err;
        callback(r);
     }
     else{
       r.res = true;
       r.insertedID = listingID;
       callback(r);
     }

 });
}


module.exports = {
   getUserRecord        : getUserRecord,
   getUserbyID          : getUserbyID,
   createUser           : createUser,
   deleteUser           : deleteUser,
   editUser             : editUser
};
