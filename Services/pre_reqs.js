let async = require('async'),
parseString = require('xml2js').parseString;

let util = require('../Utilities/util'),
pre_reqsDAO = require('../DAO/pre_reqsDAO');
//config = require("../Utilities/config").config;


/***API to get the article list */
let getpre_reqs = (callback) => {

   async.auto({
      pre_req: (cb) => {
         pre_reqsDAO.getpre_reqs({},(err, data) => {
            // console.log('data Service',data);
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.pre_req);
   })
}
// Get Category
let getpre_cat = (callback) => {

   async.auto({
      get_cat: (cb) => {
         pre_reqsDAO.getpre_cats({},(err, data) => {
            // console.log('data Service',data);
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.get_cat);
   })
}

// Get Min and Max Price
let get_price = (callback) => {

   async.auto({
      get_p: (cb) => {
         pre_reqsDAO.get_price({},(err, data) => {
            // console.log('data Service',data);
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.get_p);
   })
}

module.exports = {

   getpre_reqs : getpre_reqs,
   getpre_cat  : getpre_cat,
   get_price   : get_price

};
