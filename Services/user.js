let async = require('async'),
parseString = require('xml2js').parseString;

let util = require('../Utilities/util'),
userDAO  = require('../DAO/userDAO');

/***API to get the Users list */
// let getListing = (limit, offset, callback) => {
let getUsers = (whereClause, page, callback) => {
   async.auto({
      users: (cb) => {
         userDAO.getUserRecord({where:whereClause, page:page},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.users);
   })
}

/*** GET Listing by id to edit and stuf**////
let getUserbyID = (whereClause, callback) => {
   async.auto({
      userID: (cb) => {
         userDAO.getUserbyID({where:whereClause},(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.userID);
   })
}

let deleteUser = (data,callback) => {
   // console.log(data,'data to set')
   async.auto({
      removeUser :(cb) =>{
         if (!data) {
            cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.PARAMS_MISSING })
            return;
         }
         var criteria = {
            id : data,
         }
         userDAO.deleteUser(criteria,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DELETE_DATA });
         });
      }
   }, (err,response) => {
      callback(response.removeUser);
   });
}

let addUser = (data, callback) => {
   async.auto({
      addUser: (cb) =>{
         var dataToSet = {
            name      : data.name,
            email     : data.email,
            userName  : data.email,
            password  : data.password,
            phone     : data.phone,
            type      : data.type
         }
         // console.log('dataToSet',dataToSet);
         userDAO.createUser(dataToSet, (dbData) => {
            // console.log('dbData',dbData);
            // console.log('dbData RES',dbData.res);
            if(dbData.res == 'exist'){
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.USER_EXIST });
               return;
            }
            if (dbData.res != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.USER_EXIST });
               return;
            }
            else if(dbData.res == true){
               cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED,"result":dataToSet, "insertedID":dbData });
            }
         });
      }
   }, (err, response) => {
      callback(response.addUser);
   })
}

/*Edit User*/
let editUser = (data, callback) => {
   async.auto({
      editUserRec: (cb) =>{
         var dataToSet = [
             data.name,
             data.email,
             data.phone,
             data.type,
             data.dateModified,
             data.id
         ]
         var userID = data.id;
         // console.log('dataToSet',dataToSet);
         userDAO.editUser(dataToSet,userID, (dbData) => {
            if (dbData.res != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            else
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED,"result":dataToSet, "insertedID":dbData });
         });
      }
   }, (err, response) => {
      callback(response.editUserRec);
   })
}


module.exports = {
   getUsers        : getUsers,
   getUserbyID     : getUserbyID,
   deleteUser      : deleteUser,
   addUser         : addUser,
   editUser        : editUser
};
