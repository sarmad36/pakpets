let async   = require('async'),
parseString = require('xml2js').parseString;
let util    = require('../Utilities/util'),
sitterDAO   = require('../DAO/sitterDAO');


/***API to get the Listing list */
// let getListing = (limit, offset, callback) => {
let getListing = (whereClause, page, callback) => {
   async.auto({
      listing: (cb) => {
         sitterDAO.getListing({where:whereClause, page:page},(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.listing);
   })
}


let addListing = (data, callback) => {
   async.auto({
      addListing: (cb) =>{
         var dataToSet = {
            title        : data.title,
            category     : data.category.toString(),
            price        : data.price,
            age          : data.age,
            gender       : data.gender,
            location     : data.location,
            address      : data.address,
            description  : data.description,
            seller_name  : data.seller_name,
            seller_phone : data.seller_phone,
            seller_email : data.seller_email,
            tags         : data.tags,
            date_added   : data.date_added,
            status       : data.status,
            user_id      : data.user_id,
         }
         // console.log('dataToSet',dataToSet);
         sitterDAO.createListing(dataToSet, (dbData) => {
            // console.log('dbData CREATE',dbData);
            // console.log('dbData CREATE 11',dbData.res);
            if (dbData.res != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            else
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED,"result":dataToSet, "insertedID":dbData });
         });
      }
   }, (err, response) => {
      callback(response.addListing);
   })
}

let addListingImages = (data, listingID, callback) => {
   async.auto({
      addLisImages: (cb) =>{
         // console.log('dataToSet',dataToSet);
         sitterDAO.addListingImages(data, listingID, (dbData) => {
            console.log('imgDBdata',dbData);
            if (dbData != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED, "imageReturn":dbData });
         });
      }
   }, (err, response) => {
      callback(response.addLisImages);
   })
}

/*Edit Listing*/
let editListing = (data, callback) => {
   async.auto({
      editListingRec: (cb) =>{
         var dataToSet = [
             data.title,
             data.category.toString(),
             data.price,
             data.age,
             data.gender,
             data.location,
             data.address,
             data.description,
             data.seller_name,
             data.seller_phone,
             data.seller_email,
             data.tags,
             data.date_modified,
             data.status,
             data.user_id,
             data.id,
         ]
         var listingID = data.id;
         // console.log('dataToSet',dataToSet);
         sitterDAO.editListing(dataToSet,listingID, (dbData) => {
            if (dbData.res != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            else
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED,"result":dataToSet, "insertedID":dbData });
         });
      }
   }, (err, response) => {
      callback(response.editListingRec);
   })
}

/*** GET Listing by user id**////
let getUserListing = (whereClause, page, callback) => {
   async.auto({
      listingbyID: (cb) => {
         sitterDAO.getUserListing({where:whereClause, page:page},(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.listingbyID);
   })
}

/*** GET Listing by id to edit and stuf**////
let getListingbyID = (whereClause, callback) => {
   async.auto({
      listingID: (cb) => {
         sitterDAO.getListingbyID({where:whereClause},(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.listingID);
   })
}

/**API to delete the subject */
let deleteListing = (data,callback) => {
   // console.log(data,'data to set')
   async.auto({
      removeListing :(cb) =>{
         if (!data) {
            cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.PARAMS_MISSING })
            return;
         }
         var criteria = {
            id : data,
         }
         sitterDAO.deleteListing(criteria,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DELETE_DATA });
         });
      }
   }, (err,response) => {
      callback(response.removeListing);
   });
}

/**API to delete the subject */
let approveListing = (data,callback) => {
   // console.log(data,'data to set')
   var dataToSet = [
      'approved',
      data
   ]
   async.auto({
      apprListing :(cb) =>{
         if (!data) {
            cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.PARAMS_MISSING })
            return;
         }
         var criteria = {
            id : data,
         }
         sitterDAO.approveListing(dataToSet,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DELETE_DATA });
         });
      }
   }, (err,response) => {
      callback(response.apprListing);
   });
}

/**API to delete the subject */
let pauseListing = (data,callback) => {
   // console.log(data,'data to set')
   var dataToSet = [
      'pause',
      data
   ]
   async.auto({
      ListingPause :(cb) =>{
         if (!data) {
            cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.PARAMS_MISSING })
            return;
         }
         var criteria = {
            id : data,
         }
         sitterDAO.pauseListing(dataToSet,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DELETE_DATA });
         });
      }
   }, (err,response) => {
      callback(response.ListingPause);
   });
}


module.exports = {
   addListing        : addListing,
   addListingImages  : addListingImages,
   editListing       : editListing,
   getUserListing    : getUserListing,
   getListingbyID    : getListingbyID,
   deleteListing     : deleteListing,
   pauseListing      : pauseListing,
   approveListing    : approveListing,
   getListing        : getListing
};
