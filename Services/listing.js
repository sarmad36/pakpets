let async = require('async'),
parseString = require('xml2js').parseString;

let util = require('../Utilities/util'),
listingDAO = require('../DAO/listingDAO');

let addListing = (data, callback) => {
   async.auto({
      addListing: (cb) =>{
         var dataToSet = {
            title        : data.title,
            category     : data.category,
            price        : data.price,
            age          : data.age,
            gender       : data.gender,
            location     : data.location,
            address      : data.address,
            description  : data.description,
            seller_name  : data.seller_name,
            seller_phone : data.seller_phone,
            seller_email : data.seller_email,
            tags         : data.tags,
            date_added   : data.date_added,
            status       : data.status,
            user_id      : data.user_id,
         }
         // console.log('dataToSet',dataToSet);
         listingDAO.createListing(dataToSet, (dbData) => {
            // console.log('dbData CREATE',dbData);
            // console.log('dbData CREATE 11',dbData.res);
            if (dbData.res != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            else
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED,"result":dataToSet, "insertedID":dbData });
         });
      }
   }, (err, response) => {
      callback(response.addListing);
   })
}

let addListingImages = (data, listingID, callback) => {
   async.auto({
      addLisImages: (cb) =>{
         // console.log('dataToSet',dataToSet);
         listingDAO.addListingImages(data, listingID, (dbData) => {
            console.log('imgDBdata',dbData);
            if (dbData != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED, "imageReturn":dbData });
         });
      }
   }, (err, response) => {
      callback(response.addLisImages);
   })
}

/***API to get the listimg Count */
let getListingCount = (data, callback) => {
   async.auto({
      listingCount: (cb) => {
         listingDAO.getListingCount({},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.listingCount);
   })
}

/***API to get the Listing list */
// let getListing = (limit, offset, callback) => {
let getListing = (whereClause, page, order, callback) => {
   async.auto({
      listing: (cb) => {
         listingDAO.getListing({where:whereClause, page:page, order:order},(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.listing);
   })
}
/*** GET Listing by user id**////
let getUserListing = (whereClause, page, callback) => {
   async.auto({
      listingbyID: (cb) => {
         listingDAO.getUserListing({where:whereClause, page:page},(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.listingbyID);
   })
}

/*** GET Listing by id to edit and stuf**////
let getListingbyID = (whereClause, callback) => {
   async.auto({
      listingID: (cb) => {
         listingDAO.getListingbyID({where:whereClause},(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.listingID);
   })
}
/*Edit Listing*/
let editListing = (data, callback) => {
   async.auto({
      editListingRec: (cb) =>{
         var dataToSet = [
             data.title,
             data.category,
             data.price,
             data.age,
             data.gender,
             data.location,
             data.address,
             data.description,
             data.seller_name,
             data.seller_phone,
             data.seller_email,
             data.tags,
             data.date_modified,
             data.status,
             data.user_id,
             data.id,
         ]
         var listingID = data.id;
         // console.log('dataToSet',dataToSet);
         listingDAO.editListing(dataToSet,listingID, (dbData) => {
            if (dbData.res != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            else
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED,"result":dataToSet, "insertedID":dbData });
         });
      }
   }, (err, response) => {
      callback(response.editListingRec);
   })
}

/**API to delete the subject */
let deleteListing = (data,callback) => {
   // console.log(data,'data to set')
   async.auto({
      removeListing :(cb) =>{
         if (!data) {
            cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.PARAMS_MISSING })
            return;
         }
         var criteria = {
            id : data,
         }
         listingDAO.deleteListing(criteria,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DELETE_DATA });
         });
      }
   }, (err,response) => {
      callback(response.removeListing);
   });
}

/***API to get the listimg Count */
let getListingAdmin = (page, status, callback) => {
   // console.log(".llkll,k");
   async.auto({
      listingAdmin: (cb) => {
         listingDAO.getListingAdmin({page:page, status:status},(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.listingAdmin);
   })
}

/**API to delete the subject */
let approveListing = (data,callback) => {
   // console.log(data,'data to set')
   var dataToSet = [
      'approved',
      data
   ]
   async.auto({
      apprListing :(cb) =>{
         if (!data) {
            cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.PARAMS_MISSING })
            return;
         }
         var criteria = {
            id : data,
         }
         listingDAO.approveListing(dataToSet,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DELETE_DATA });
         });
      }
   }, (err,response) => {
      callback(response.apprListing);
   });
}


/**API to pause  */
let pauseListing = (data,callback) => {
   // console.log(data,'data to set')
   var dataToSet = [
      'pause',
      data
   ]
   async.auto({
      ListingPause :(cb) =>{
         if (!data) {
            cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.PARAMS_MISSING })
            return;
         }
         var criteria = {
            id : data,
         }
         listingDAO.pauseListing(dataToSet,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DELETE_DATA });
         });
      }
   }, (err,response) => {
      callback(response.ListingPause);
   });
}

/*change password*/
let changePassword = (userID,password,callback) => {
   // console.log(data,'data to set')
   var dataToSet = [
      password,
      userID
   ]
   async.auto({
      passwordChange :(cb) =>{
         listingDAO.changePassword(dataToSet,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED });
         });
      }
   }, (err,response) => {
      callback(response.passwordChange);
   });
}

let sendMessage = (data, callback) => {
   async.auto({
      messageSend: (cb) =>{
         // console.log('dataToSet',dataToSet);
         var res = "success"
         listingDAO.sendMessage(data, (dbData) => {
            if (dbData.res != true) {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            else
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DATA_UPDATED,"result":res });
         });
      }
   }, (err, response) => {
      callback(response.messageSend);
   })
}

let forgotPassword = (data, callback) => {
   async.auto({
      passwordForgot: (cb) =>{
         // console.log('dataToSet',dataToSet);
         var sucess = "success";
         listingDAO.forgotPassword(data, (dbData) => {
            console.log("SERVICE Response", dbData);
            if (dbData.res == 'No email') {
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_FOUR, "statusMessage": util.statusMessage.NOUSER_PASS});
               return;
            }
            if (dbData.res == true) {
               cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.PASS_UPDATE,"result":sucess });
            }
            else{
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }

         });
      }
   }, (err, response) => {
      callback(response.passwordForgot);
   })
}

let getChat = (whereClause, callback) => {
   async.auto({
      chat: (cb) => {
         listingDAO.getChat(whereClause,(err, data) => {
         // listingDAO.getListing({limit: limit, offset: offset},(err, data) => {
            if (err) {
               cb(null, {"errorCode": util.statusCode.INTERNAL_SERVER_ERROR,"statusMessage": util.statusMessage.SERVER_BUSY});
               return;
            }
            cb(null, data);
            return;
         });
      }
   }, (err, response) => {
      callback(response.chat);
   })
}

/**Add Likes  */
let addLikes = (id,callback) => {
   // console.log(data,'data to set')
   // var dataToSet = [
   //    `likes`+1,
   //    id
   // ]
   var dataToSet = [
      id
   ]
   async.auto({
      ListingLikes :(cb) =>{
         if (!id) {
            cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.PARAMS_MISSING })
            return;
         }
         listingDAO.addLikes(dataToSet,(err,dbData) => {
            if (err) {
               console.log(err);
               cb(null, { "statusCode": util.statusCode.FOUR_ZERO_ONE, "statusMessage": util.statusMessage.SERVER_BUSY });
               return;
            }
            cb(null, { "statusCode": util.statusCode.OK, "statusMessage": util.statusMessage.DELETE_DATA });
         });
      }
   }, (err,response) => {
      callback(response.ListingLikes);
   });
}

module.exports = {
   addListing        : addListing,
   getListing        : getListing,
   getListingCount   : getListingCount,
   addListingImages  : addListingImages,
   getUserListing    : getUserListing,
   getListingbyID    : getListingbyID,
   editListing       : editListing,
   deleteListing     : deleteListing,
   getListingAdmin   : getListingAdmin,
   approveListing    : approveListing,
   pauseListing      : pauseListing,
   changePassword    : changePassword,
   sendMessage       : sendMessage,
   forgotPassword    : forgotPassword,
   getChat           : getChat,
   addLikes          : addLikes
   // approveListing    : approveListing
   // getArticle : getArticle,
   // getArticleById : getArticleById
};
