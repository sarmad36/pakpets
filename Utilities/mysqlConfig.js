var config = require("../Utilities/config").config;
var mysql = require('mysql');
// var connection = mysql.createConnection({
var connection = mysql.createPool({
   host: config.DB_URL_MYSQL.host,
   user: config.DB_URL_MYSQL.user,
   password: config.DB_URL_MYSQL.password,
   database: config.DB_URL_MYSQL.database,
});
// var connection;
// function connectDb() {
//   connection  = mysql.createConnection({
//     host: config.DB_URL_MYSQL.host,
//       user: config.DB_URL_MYSQL.user,
//       password: config.DB_URL_MYSQL.password,
//       database: config.DB_URL_MYSQL.database,
//   });
//   connection.on('error', connectDb()); // probably worth adding timeout / throttle / etc
// }

// connectDb();

// connection.connect(() => {
//    require('../Models/Article').initialize();
// });

let getDB = () => {
   return connection;
}

module.exports = {
  // getDB: connectDb
   getDB: getDB
}
