let app        = require('express')(),
server         = require('http').Server(app),
bodyParser     = require('body-parser')
express        = require('express'),
cors           = require('cors'),
http           = require('http'),
path           = require('path');
var multer     = require('multer');
const session  = require('express-session');

let articleRoute  = require('./Routes/article')
let listingRoute  = require('./Routes/listing')
let pre_reqsRoute = require('./Routes/pre_reqs')
let userRoute     = require('./Routes/user')
let sitterRoute   = require('./Routes/sitter')
let loginRoute    = require('./Routes/login'),
util = require('./Utilities/util');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false }));

// app.use(cors());
app.use(cors({origin: [
  "http://localhost:4200"
], credentials: true}));

/**
 * Initializing the session magic of express-session package
 */
app.use(session({
  secret: "Shh, its a secret!",
  resave: false,
  saveUninitialized: true
}));

// // catch 404 and forward to error handler
app.use(function(req, res, next) {
   next();
});

app.use(function(err, req, res, next) {
   return res.send({ "statusCode": util.statusCode.ONE, "statusMessage": util.statusMessage.SOMETHING_WENT_WRONG });
});

app.use('/article', articleRoute);
app.use('/listing', listingRoute);
app.use('/pre_reqs', pre_reqsRoute);
app.use('/user', userRoute);
app.use('/sitter', sitterRoute);
app.use('/login', loginRoute);

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//    next();
// });

/*first API to check if server is running*/
app.get('*', (req, res) => {
   res.sendFile(path.join(__dirname, '../server/client/dist/index.html'));
})

server.listen(3000,function(){
   console.log('app listening on port: 3000');
});
