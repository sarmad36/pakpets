let express       =  require('express');
router            = express.Router();
let dbConfig      = require("../Utilities/mysqlConfig");
util              = require('../Utilities/util');
// listingService    = require('../Services/login');
const cors        = require('cors');
const bodyParser  = require('body-parser');
const paginate    = require('jw-paginate');
var multer        = require('multer');
var fs            = require('fs');
const session     = require('express-session');
const bcrypt      = require('bcrypt');
var USERDATA      ={};

/**
 * Some hardcoded users to make the demo work
 */
const appUsers = {
  'max@gmail.com': {
    email: 'max@gmail.com',
    name: 'Max Miller',
    pw: '1234' // YOU DO NOT WANT TO STORE PW's LIKE THIS IN REAL LIFE - HASH THEM FOR STORAGE
  },
  'lily@gmail.com': {
    email: 'lily@gmail.com',
    name: 'Lily Walter',
    pw: '1235' // YOU DO NOT WANT TO STORE PW's LIKE THIS IN REAL LIFE - HASH THEM FOR STORAGE
  }
};


router.post('/do-login', (req, res) => {
   var response = '';
   // console.log('email', req.body.email);
   // console.log('password', req.body.password);
   const user     = appUsers[req.body.email];


   // console.log("query",'SELECT * FROM users WHERE email = "'+req.body.email+'"');
   dbConfig.getDB().query('SELECT * FROM users WHERE email = "'+req.body.email+'"', function(err, result) {
      if (err) {
         res.status(403).send({
           errorMessage: 'Permission denied!'
         });
      }
      else if(Object.keys(result).length === 0 ) {
         res.status(403).send({
           errorMessage: 'Permission denied!'
         });
      }
      else {
         // console.log('res',result);
         // if (err) throw err;
         // console.log("selected user",result);
         if(bcrypt.compareSync(req.body.password, result[0].password)) {
            const userWithoutPassword = {result};
            // USERDATA = userWithoutPassword.result[0];
            // delete userWithoutPassword.pw;
            req.session.user = userWithoutPassword;
            res.status(200).send({
              user: userWithoutPassword.result[0],
            });
         } else {
            res.status(403).send({
              errorMessage: 'Permission denied!'
            });
         }

      }

   });
  // if (user && user.pw === req.body.password) {
  //   const userWithoutPassword = {user};
  //   // delete userWithoutPassword.pw;
  //   req.session.user = userWithoutPassword;
  //   res.status(200).send({
  //     user: userWithoutPassword
  //   });
  // } else {
  //   res.status(403).send({
  //     errorMessage: 'Permission denied!'
  //   });
  // }
});

/**
 * Check if user is logged in.
 */
router.get('/check-login', (req, res) => {
  req.session.user ? res.status(200).send({loggedIn: true, userdata: req.session.user}) : res.status(200).send({loggedIn: false});
});

/**
 * Log the user out of the application.
 */
router.post('/logout', (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res.status(500).send('Could not log out.');
    } else {
      // USERDATA = [];
      res.status(200).send({loggedOut: true});
    }
  });
});


module.exports = router;
