let express       =  require('express');
router            = express.Router();
util              = require('../Utilities/util');
listingService    = require('../Services/listing');
const cors        = require('cors');
const bodyParser  = require('body-parser');
const paginate    = require('jw-paginate');
var multer        = require('multer');
var fs            = require('fs');
const bcrypt      = require('bcrypt');


/* Add Listing */
router.post('/add-listing', (req, res) => {
   // console.log('req',req.body.params.saveData);
   listingService.addListing(req.body.params.saveData, (data) => {
      // console.log("Add listing response",data);
      // console.log("Add listing response",data.insertedID.insertedID);
      // res.send(data);
      if (req.body.params.saveData.images != undefined || req.body.params.saveData.images != null || req.body.params.saveData.images.length === 0) {
         listingService.addListingImages(req.body.params.saveData.images, data.insertedID.insertedID, (data) => {
            // console.log("imgesResp",data.imageReturn);
            // console.log("IDDDDD",data.insertedID.insertId);
            res.send(data.imageReturn);
         });
      }
      else {
         res.send(true);
      }

   });
});

/**Api to get the list of article */
router.post('/get-listing', (req, res) => {
   var totalRec      = 0,
   pageSize          = 10,
   pageCount         = 0;
   var start         = 0;
   var currentPage   = 1;
   var category      = req.body.params.saveData.category;
   var location      = req.body.params.saveData.location;
   var price         = req.body.params.saveData.price;
   let whereClause   = '';
   let order         = '';
   // console.log("req.body.params.saveData.order",req.body.params.saveData.order);
   // console.log("req.body.params.saveData",req.body.params.saveData.category);
   if (price == '`price`') {
      // console.log('if',price);
      whereClause = '`category` = '+category+' AND `location` = '+location+' AND `price` = '+price+' AND `status` = "approved"';
   }
   else {
      whereClause = '`category` = '+category+' AND `location` = '+location+' AND '+price+' AND `status` = "approved"';
      // console.log('else',price);
   }
   if (req.body.params.saveData.order == undefined || req.body.params.saveData.order == '') {
      order = 'l.`id`'
   }
   else if (req.body.params.saveData.order == 'likes') {
      order = 'l.`likes`'
   }
   else{
      order = 'l.`id`'
   }
   // console.log('whereClause',whereClause);
  //////// listingService.getListing(req.query, (data) => {
   listingService.getListing(whereClause,req.body.params.page,order, (data) => {
    const items = data;
    // console.log("items",items);
    // get page from query params or default to first page
    const page = parseInt(req.body.params.page) || 1;
    // get pager object for specified page
    const pager = paginate(items.length, page);
    // get page of items from items array
    const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);
    // return pager object and current page of items

    //////////new lists
    order            = 'l.`id`';
    // let newlisting   ={};
    listingService.getListing(whereClause,req.body.params.page,order, (data) => {
    const item1      = data;
    const page1      = 1;
    const pager1     = paginate(item1.length, page1);
    const newlisting = item1.slice(pager1.startIndex, pager1.endIndex + 1);
    return res.json({ pager, pageOfItems, newlisting});
   });
    // return res.json({ pager, pageOfItems, newlisting});
      // res.send(data);
   });
});

/* Edit Listing */
router.post('/edit-listing', (req, res) => {
   var im = [];
   for (var i = 0; i < req.body.params.saveData.images.length; i++) {
      im.push({
         'saved': req.body.params.saveData.images[i]
      });
   }
   // console.log('im',im);
   listingService.editListing(req.body.params.saveData, (data) => {
      // console.log("data",data);
      // console.log("images",req.body.params.saveData.images);
      listingService.addListingImages(im, data.insertedID.insertedID, (data) => {
         res.send(data.imageReturn);
      });
   });
});

// /**Api to delete 1 image */
router.delete('/delete-listing', (req, res) => {
  console.log('query',req.query.delData);
  listingService.deleteListing(req.query, (data) => {
     return res.json({resp: true});
  });

});

var storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
           // console.log("destination",file);
            cb(null, '../listing/');
        },
        filename: function (req, file, cb) {
           // console.log("filename",file);
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
        }
    });

 var storage_temp = multer.diskStorage({ //multers disk storage settings
         destination: function (req, file, cb) {
            // console.log("destination",file);
             cb(null, './temp/temp/');
         },
         filename: function (req, file, cb) {
            // console.log("filename",file);
             var datetimestamp = Date.now();
             cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
         }
     });

    var upload = multer({ //multer settings
                    storage: storage
                }).single('file');

    var upload_temp = multer({ //multer settings
                    storage: storage_temp
                }).single('file');

    /** API path that will upload the files */
    router.post('/upload', (req, res) => {
      // console.log("11111111111111",req);
      // console.log("filesssss",req.files);
        upload(req,res,function(err){
			// console.log("file",req.file);
            if(err){
                 res.json({error_code:1,err_desc:err});
                 return;
            }
             res.json({error_code:0,err_desc:null,savedname:req.file});
        });
    });

    /** API path that will upload the files Temporaroly*/
  router.post('/temp-upload', (req, res) => {
     console.log("filesssss",req);
      upload_temp(req,res,function(err){
        console.log("file",req.file);
           if(err){
                res.json({error_code:1,err_desc:err});
                return;
           }
           res.json({error_code:0,err_desc:null,savedname:req.file});
      });
  });
  // router.post("/temp-upload", multer({dest: "./temp/temp/"}).array("uploads", 12), function(req, res) {
//     // res.send(req.files);
//     console.log(req.files);
// });

    // /**Api to delete 1 image */
    router.delete('/remove-img', (req, res) => {
      // console.log('req',req.body.params.delData);
      // console.log('query',req.query);

      var data = JSON.parse(req.query.delData)
      // console.log('data',data);
      fs.unlink('../listing/'+data.new, function (err) {
         if (err) throw err;
         // if no error, file has been deleted successfully
         res.json({status:true});
      });

    });

    // /**Api to delete all images */
    router.delete('/remove-all-img', (req, res) => {
      // console.log('req',req.body.params.delData);
      // console.log('query',req.query);

      var data = JSON.parse(req.query.delData)
      //console.log('data',data);
      var count = 0;
      for (var i = 0; i < data.length; i++) {
         fs.unlink('../listing/'+data[i].saved, function (err) {
            if (err) throw err;
            // if no error, file has been deleted successfully
         });
         count++;
      }
      // console.log("count",count);
      if (count == data.length) {
         // console.log("yess");
         res.json({status:true});
      }
      else {
         res.json({status:false});
      }


    });
// /* Add Listing */
// router.post('/temp-img', (req, res) => {
//    console.log('req',req);
//    console.log('req file',req.files);
//    // listingService.addListing(req.body.params.saveData, (data) => {
//    //    console.log("Add listing response",data);
//    //    res.send(data);
//    // });
// });

/**Api to get the list of article */
router.post('/get-users-listing', (req, res) => {
   var totalRec      = 0,
   pageSize          = 10,
   pageCount         = 0;
   var start         = 0;
   var currentPage   = 1;
   var id            = req.body.params.saveData;
   let whereClause   = '';
   whereClause       = '`user_id` = '+id;
   // console.log("req.body.params",req.body.params);
   listingService.getUserListing(whereClause,req.body.params.page, (data) => {
    const items = data;
    // console.log("items",items);
    const page = parseInt(req.body.params.page) || 1;
    const pager = paginate(items.length, page);
    const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

    return res.json({ pager, pageOfItems });
      // res.send(data);
   });
});

router.get('/get-listing-byID', (req, res) => {

   var id            = req.query.id;
   // let whereClause   = '';
   whereClause       = 'l.id = '+id;
   // console.log('query',req.query.id);


   listingService.getListingbyID(whereClause, (data) => {
    const items = data;
    // console.log("items",items);
    return res.json(items);
      // res.send(data);
   });
});

/**Api to get the listings for admin */
router.post('/get-listing-admin', (req, res) => {
   var totalRec      = 0,
   pageSize          = 10,
   pageCount         = 0;
   var start         = 0;
   var currentPage   = 1;
   let whereClause   = '';
   let status        = req.body.params.saveData.status;
   // console.log('status',status);
   if (req.body.params.saveData.status == '`status`') {
      whereClause = '`status` = `status`';
   }
   else {
      whereClause = '`status` = "'+ req.body.params.saveData.status+'"';
   }
   console.log('req.body.params',req.body.params);
   // console.log("req.body.params.saveData",req.body.params.saveData.category);
   // console.log("getListingAdmingetListingAdmin");
   listingService.getListingAdmin(req.body.params.page, whereClause, (data) => {
    const items = data;
    // console.log("items",items);
    const page = parseInt(req.body.params.page) || 1;
    const pager = paginate(items.length, page);
    const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);
    return res.json({ pager, pageOfItems });
      // res.send(data);
   });
});

// /**Api to delete 1 image */
router.put('/approve-listing', (req, res) => {
  console.log('query',req.body.params.updateData);
  listingService.approveListing(req.body.params.updateData, (data) => {
     return res.json(true);
  });

});

// /**Api to  activate*/
router.post('/approve-listing', (req, res) => {
  // console.log('query',req.body.params.saveData);
  listingService.approveListing(req.body.params.saveData, (data) => {
     return res.json(true);
  });

});

// /**Api to pause */
router.post('/pause-listing', (req, res) => {
  // console.log('query',req.body.params.saveData);
  listingService.pauseListing(req.body.params.saveData, (data) => {
     return res.json(true);
  });

});

/* Change Password */
router.post('/change-password', (req, res) => {
   console.log('req',req.body.params.saveData);
   var userID   = req.body.params.saveData.userID;
   var password = bcrypt.hashSync(req.body.params.saveData.password, 10);;
   listingService.changePassword(userID,password, (data) => {
      console.log("Add listing response",data);
      // console.log("Add listing response",data.insertedID.insertedID);
      // res.send(data);
         res.send(true);

   });
});

/* Add Listing */
router.post('/send-message', (req, res) => {
   // console.log('req',req.body.params.saveData);
   listingService.sendMessage(req.body.params.saveData, (data) => {
      console.log("data",data);
      if (data.result == "success") {
         res.json({status: 1, message: 'mail sent'});
      }
      else {
         res.json({status: 0, message: 'mail not sent'});
      }

   });
});

/* Add Listing */
router.post('/forgot-password', (req, res) => {
   // console.log('forgot password',req.body.params.saveData.email);
   listingService.forgotPassword(req.body.params.saveData.email, (data) => {
      // console.log("data",data.statusMessage);
      if(data.statusMessage == 'Password Updated'){
         res.json({status: 1, message: 'updated'});
      }
      else if (data.statusMessage == 'Email Does Not exist') {
         res.json({status: 0, message: 'onemial'});
      }
      else {
         res.json({status: 2, message: 'error'});
      }


   });
});

router.post('/get-chats', (req, res) => {

   let whereClause   = 'c.`senderEmail` = "'+req.body.params.saveData.userEmail+'" OR c.`receiverEmail` = "'+req.body.params.saveData.userEmail+'"';
   // console.log("req.body.params.saveData",req.body.params.saveData.category);
   // console.log('whereClause',whereClause);
   listingService.getChat(whereClause, (data) => {
      console.log('data',data);
      if (data.length == 0 ) {
         return res.json({response: false, data: data});
      }
      else {
         return res.json({response: true, data: data});
      }

      // res.send(data);
   });
});

/**Api to get the list of article */
router.post('/add-likes', (req, res) => {

   // console.log("req.body.params.saveData",req.body.params.saveData);
   listingService.addLikes(req.body.params.saveData, (data) => {
    // return res.json();
      res.send(true);
   });
});


module.exports = router;

// console.log('req.Body',req.body.params.saveData);
// console.log('req',req);
//  listingService.getListingCount(req.body.params.saveData, (data) => {
//     // console.log("count",data); ////// data  = productsCount
//     // console.log("data[0].totalListing",data[0].totalListing);
//     totalRec      = data[0].totalListing;
//     pageCount     =  Math.ceil(totalRec /  pageSize);
//     // console.log('totalRec',totalRec);
//     // console.log('pageCount',pageCount);
//     if (typeof req.body.params.saveData !== 'undefined') {
//        currentPage = req.body.params.saveData;
//     }
//     if(currentPage >1){
//
//        start = (currentPage - 1) * pageSize;
//     }
//     console.log('offset',start);
//     console.log('limit',pageCount);
//     listingService.getListing(pageSize, start, (data) => {
//        console.log('pageSize',pageSize);
//        console.log('pageCount',pageCount);
//        console.log('currentPage',currentPage);
//        // console.log("Add listing response",data);
//        // res.send(data);
//        // const items = data;
//        const items = data;
//        const page = parseInt(currentPage) || 1;
//        const pager = paginate(data.length, page);
//        const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);
//        // console.log('pageOfItems',pageOfItems);
//        console.log('pager',pager);
//        // return res.json({ pager, pageOfItems });
//        ////// pageSize = pageSize
//        ////// pageCount = totalpages
//        ////// start page = currentPage
//        ///// end page = pageSize
//        return res.json({ data: data, pageSize: pageSize, pageCount: pageCount,currentPage: currentPage});
//        // res.send({ data: data, pageSize: pageSize, pageCount: pageCount,currentPage: currentPage});
//     });
//     // var result = req.models.products.find({},{limit: pageSize, offset: start}, function(error, products){
//     // res.render('index', { data: products, pageSize: pageSize, pageCount: pageCount,currentPage: currentPage});
// // });
//   // return res.json({ pager, pageOfItems });
//  });
