let express       =  require('express');
router            = express.Router();
util              = require('../Utilities/util');
sitterService     = require('../Services/sitter');
const cors        = require('cors');
const bodyParser  = require('body-parser');
const paginate    = require('jw-paginate');
var multer        = require('multer');
var fs            = require('fs');
const bcrypt      = require('bcrypt');


/**Api to get the list of article */
router.post('/get-listing', (req, res) => {
   var totalRec      = 0,
   pageSize          = 10,
   pageCount         = 0;
   var start         = 0;
   var currentPage   = 1;
   var category      = req.body.params.saveData.category;
   var location      = req.body.params.saveData.location;
   var price         = req.body.params.saveData.price;
   let whereClause   = '';
   // console.log("req.body.params.saveData",req.body.params.saveData.category);
   if (price == '`price`') {
      // console.log('if',price);
      whereClause = '`category` = '+category+' AND `location` = '+location+' AND `price` = '+price;
   }
   else {
      whereClause = '`category` = '+category+' AND `location` = '+location+' AND '+price;
      // console.log('else',price);
   }
   // console.log('whereClause',whereClause);
  //////// listingService.getListing(req.query, (data) => {
   sitterService.getListing(whereClause,req.body.params.page, (data) => {
    const items = data;
    // console.log("items",items);
    // get page from query params or default to first page
    const page = parseInt(req.body.params.page) || 1;
    // get pager object for specified page
    const pager = paginate(items.length, page);
    // get page of items from items array
    const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);
    // return pager object and current page of items
    return res.json({ pager, pageOfItems });
      // res.send(data);
   });
});


/* Add Listing */
router.post('/add-listing', (req, res) => {
   // console.log('req',req.body.params.saveData);
   sitterService.addListing(req.body.params.saveData, (data) => {
      // console.log("Add listing response",data);
      // console.log("Add listing response",data.insertedID.insertedID);
      // res.send(data);
      if (req.body.params.saveData.images != undefined || req.body.params.saveData.images != null || req.body.params.saveData.images.length === 0) {
         sitterService.addListingImages(req.body.params.saveData.images, data.insertedID.insertedID, (data) => {
            // console.log("imgesResp",data.imageReturn);
            // console.log("IDDDDD",data.insertedID.insertId);
            res.send(data.imageReturn);
         });
      }
      else {
         res.send(true);
      }

   });
});

/* Edit Listing */
router.post('/edit-listing', (req, res) => {
   var im = [];
   for (var i = 0; i < req.body.params.saveData.images.length; i++) {
      im.push({
         'saved': req.body.params.saveData.images[i]
      });
   }
   // console.log('im',im);
   sitterService.editListing(req.body.params.saveData, (data) => {
      // console.log("data",data);
      // console.log("images",req.body.params.saveData.images);
      sitterService.addListingImages(im, data.insertedID.insertedID, (data) => {
         res.send(data.imageReturn);
      });
   });
});

/**Api to get the list of article */
router.post('/get-users-listing', (req, res) => {
   var totalRec      = 0,
   pageSize          = 10,
   pageCount         = 0;
   var start         = 0;
   var currentPage   = 1;
   var id            = req.body.params.saveData.userID;
   var userType      = req.body.params.saveData.userType;
   let whereClause   = '';
   if (userType == 1 || userType == 2) {
      whereClause       = 1;
   }
   else {
      whereClause       = '`user_id` = '+id;
   }
   // console.log("req.body.params",req.body.params);
   sitterService.getUserListing(whereClause,req.body.params.page, (data) => {
    const items = data;
    // console.log("items",items);
    const page = parseInt(req.body.params.page) || 1;
    const pager = paginate(items.length, page);
    const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

    return res.json({ pager, pageOfItems });
      // res.send(data);
   });
});

router.get('/get-listing-byID', (req, res) => {

   var id            = req.query.id;
   // let whereClause   = '';
   whereClause       = 'l.id = '+id;
   // console.log('query',req.query.id);


   sitterService.getListingbyID(whereClause, (data) => {
    const items = data;
    // console.log("items",items);
    return res.json(items);
      // res.send(data);
   });
});

router.delete('/delete-listing', (req, res) => {
  console.log('query',req.query.delData);
  sitterService.deleteListing(req.query, (data) => {
     return res.json(true);
  });

});

// /**Api to  activate*/
router.post('/approve-listing', (req, res) => {
  // console.log('query',req.body.params.saveData);
  sitterService.approveListing(req.body.params.saveData, (data) => {
     return res.json(true);
  });

});


// /**Api to delete 1 image */
router.post('/pause-listing', (req, res) => {
  // console.log('query',req.body.params.saveData);
  sitterService.pauseListing(req.body.params.saveData, (data) => {
     return res.json(true);
  });

});


var storage = multer.diskStorage({ //multers disk storage settings
        destination: function (req, file, cb) {
           // console.log("destination",file);
            cb(null, '../listing/sitter/');
        },
        filename: function (req, file, cb) {
           // console.log("filename",file);
            var datetimestamp = Date.now();
            cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
        }
    });

var upload = multer({ //multer settings
                storage: storage
            }).single('file');

/** API path that will upload the files */
router.post('/upload', (req, res) => {
  // console.log("11111111111111",req);
  // console.log("filesssss",req.files);
    upload(req,res,function(err){
     // console.log("file",req.file);
        if(err){
             res.json({error_code:1,err_desc:err});
             return;
        }
         res.json({error_code:0,err_desc:null,savedname:req.file});
    });
});

// /**Api to delete 1 image */
router.delete('/remove-img', (req, res) => {
  // console.log('req',req.body.params.delData);
  // console.log('query',req.query);

  var data = JSON.parse(req.query.delData)
  // console.log('data',data);
  fs.unlink('../listing/sitter/'+data.new, function (err) {
     if (err) throw err;
     // if no error, file has been deleted successfully
     res.json({status:true});
  });

});

// /**Api to delete all images */
router.delete('/remove-all-img', (req, res) => {
  // console.log('req',req.body.params.delData);
  // console.log('query',req.query);

  var data = JSON.parse(req.query.delData)
  //console.log('data',data);
  var count = 0;
  for (var i = 0; i < data.length; i++) {
     fs.unlink('../listing/sitter/'+data[i].saved, function (err) {
        if (err) throw err;
        // if no error, file has been deleted successfully
     });
     count++;
  }
  // console.log("count",count);
  if (count == data.length) {
     // console.log("yess");
     res.json({status:true});
  }
  else {
     res.json({status:false});
  }


});

module.exports = router;
