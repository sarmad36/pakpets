let express       =  require('express');
router            = express.Router();
util              = require('../Utilities/util');
userService       = require('../Services/user');
const cors        = require('cors');
const bodyParser  = require('body-parser');
const paginate    = require('jw-paginate');
var multer        = require('multer');
var fs            = require('fs');
const bcrypt      = require('bcrypt');

/**Api to get the list of article */
router.post('/get-users', (req, res) => {
   var totalRec      = 0,
   pageSize          = 10,
   pageCount         = 0;
   var start         = 0;
   var currentPage   = 1;

   let whereClause   = 1;

   userService.getUsers(whereClause,req.body.params.page, (data) => {
    const items = data;
    const page = parseInt(req.body.params.page) || 1;
    // get pager object for specified page
    const pager = paginate(items.length, page);
    // get page of items from items array
    const pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);
    // return pager object and current page of items
    return res.json({ pager, pageOfItems });
      // res.send(data);
   });
});

router.get('/get-user-byID', (req, res) => {

   var id            = req.query.id;
   // let whereClause   = '';
   whereClause       = 'id = '+id;
   // console.log('query',req.query.id);


   userService.getUserbyID(whereClause, (data) => {
    const items = data;
    // console.log("items",items);
    return res.json(items);
      // res.send(data);
   });
});

router.delete('/delete-user', (req, res) => {
  console.log('query',req.query.delData);
  userService.deleteUser(req.query, (data) => {
     return res.json(true);
  });

});

router.post('/add-user', (req, res) => {
   // console.log('req',req.body.params.saveData);

   userService.addUser(req.body.params.saveData, (data) => {
      // console.log('data',data);
      if (data.statusMessage == 'User already Exist') {
         res.json(false);
      }
      else {
         res.json(true);
      }
      // console.log("Add listing response",data);
      // console.log("Add listing response",data.insertedID.insertedID);
         // res.send(true);

   });
});

router.post('/edit-user', (req, res) => {
   // console.log('im',im);
   userService.editUser(req.body.params.saveData, (data) => {
      console.log("data",data);
      // console.log("images",req.body.params.saveData.images);
         // res.send(true);
   });
});

module.exports = router;
